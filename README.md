Made in Unity 2017.3.0

This project contains a WebGL Template that removes the wrapper around a WebGL build. This removes full-screen button.

View the [Wiki](https://gitlab.com/aegodbold/webgl-template-remove-wrapper/wikis/how-to-setup) for instructions on setup.


